MAKEFLAGS += --no-print-directory

.PHONY: default
.PHONY: installdeps
.PHONY: both

default: main

both: installdeps main

include arch.mk
include install.mk
include run.mk
-include .dir.mk

deps += ../libjson
deps += ../libarray
deps += ../libdebug
deps += ../libchar
deps += ../libcstring
deps += ../libavl

deps_a = $(foreach dep,$(deps),$(dep).a)

.%.c.o: %.c
	$(CC) -c $(CPPFLAGS) $(CFLAGS) $< -o $@ || ($(EDITOR) $< && false)
.%.cpp.o: %.cpp
	$(CXX) -c $(CPPFLAGS) $(CXXFLAGS) $< -o $@
.dir.o:
	$(LD) -r $^ -o $@
%/.dir.o:
	$(LD) -r $^ -o $@

main: $(deps_a) .dir.o
	$(CC) $(LDFLAGS) .dir.o $(deps_a) $(LOADLIBES) $(LDLIBS) -o main

installdeps: cmd = $(MAKE) -C $(dep) install &&
installdeps:
	$(foreach dep,$(deps),$(cmd)) true

clean:
	find . -type f -name '*.o' | xargs rm -vf main
