
#include <stdio.h>
#include <assert.h>

#include "libavl.h"
#include "libarray.h"
#include "libjson.h"
#include "libdebug.h"

int depth = 0;

void pretty_print(struct json* json, bool ident)
{
	switch(json->kind)
	{
		case jk_boolean:
		{
			struct json_boolean* spef = json;
			if(ident)
			for(int i = depth;i--;) printf("\t");
			if(spef->value)
			{
				printf("true");
			}
			else
			{
				printf("false");
			}
			break;
		}
		case jk_number:
		{
			struct json_number* spef = json;
			if(ident)
			for(int i = depth;i--;) printf("\t");
			if(spef->is_double)
			{
				printf("%f", spef->decimal);
			}
			else
			{
				printf("%li", spef->integer);
			}
			break;
		}
		case jk_string:
		{
			struct json_string* spef = json;
			if(ident)
			for(int i = depth;i--;) printf("\t");
			printf("\"%s\"", spef->chars.data);
			break;
		}
		case jk_list:
		{
			struct json_list* spef = json;
			if(ident)
			for(int i = depth;i--;) printf("\t"); printf("[\n");
			depth++;
			for(int i = 0, n = spef->elements.n;i < n;)
			{
				struct json_object* ele =
					array_index(spef->elements, i, struct json_object*);
				pretty_print(ele, true);
				if(++i < n)
				{
					printf(",\n");
				}
			}
			printf("\n");
			depth--;
			for(int i = depth;i--;) printf("\t"); printf("]");
			break;
		}
		case jk_object:
		{
			struct json_object* spef = json;
			if(ident)
			for(int i = depth;i--;) printf("\t"); printf("{\n");
			depth++;
			for(
				struct avl_node* cursor = spef->values.leftmost;
				cursor;
				cursor = cursor->next
			)
			{
				struct json_object_value* ele = cursor->item;
				for(int i = depth;i--;) printf("\t"); 
				printf("\"%s\": ", ele->name.data);
				pretty_print(ele->value, false);
				if(cursor->next)
				{
					printf(",\n");
				}
			}
			printf("\n");
			depth--;
			for(int i = depth;i--;) printf("\t"); printf("}");
			break;
		}
		default: TODO;
	}
}

int next(FILE* ptr, char* c)
{
	return fread(c, 1, 1, ptr);
}

void push(FILE* ptr, char* c, int n)
{
	return fwrite(c, n, 1, ptr);
}

int main()
{
	ENTER;
	FILE* f = fopen("example.json", "r");
	verpv(f);
	char c;
	assert(next(f, &c));
	struct json* json = json_scan(f, next, &c);
	verpv(json);
	// pretty_print(json, true), printf("\n");
	json_print(json, stdout, push);
	json_free(json);
	fclose(f);
	EXIT;
	return 0;
}








